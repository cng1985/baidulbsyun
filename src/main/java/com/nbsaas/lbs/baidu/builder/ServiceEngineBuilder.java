package com.nbsaas.lbs.baidu.builder;

import com.nbsaas.lbs.baidu.v3.service.Config;

public class ServiceEngineBuilder {
  
  private String ak;
  
  private String geotable;
  
  private ServiceEngineBuilder(){
  }
  
  public ServiceEngine build() {
    Config config=new Config();
    config.setAk(this.ak);
    config.setGeotable(this.geotable);
    ServiceEngine result = new ServiceEngine(config);
    return result;
  }
  
  public static ServiceEngineBuilder newBuilder() {
    ServiceEngineBuilder result = new ServiceEngineBuilder();
    return result;
  }
  
  public ServiceEngineBuilder ak(String ak) {
    this.ak = ak;
    return this;
  }
  public ServiceEngineBuilder geotable(String geotable) {
    this.geotable = geotable;
    return this;
  }
}
