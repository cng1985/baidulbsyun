package com.nbsaas.lbs.baidu.builder;

import com.nbsaas.lbs.baidu.ip.service.IpService;
import com.nbsaas.lbs.baidu.ip.service.impl.IPServiceImpl;
import com.nbsaas.lbs.baidu.v3.api.LbsSearchService;
import com.nbsaas.lbs.baidu.v3.api.PoiService;
import com.nbsaas.lbs.baidu.v3.service.Config;
import com.nbsaas.lbs.baidu.v3.service.LbsSearchServiceImpl;
import com.nbsaas.lbs.baidu.v3.service.PoiServiceImpl;

public class ServiceEngine {
  
  public ServiceEngine(Config config) {
    this.config = config;
  }
  
  private Config config;
  
  public PoiService getV3PoiService(){
    Config config=new Config();
    config.setAk(this.config.getAk());
    config.setGeotable(this.config.getGeotable());
    PoiServiceImpl service=new PoiServiceImpl(config);
    return service;
  }
  
  public LbsSearchService getV3LbsSearchService(){
    Config config=new Config();
    config.setAk(this.config.getAk());
    config.setGeotable(this.config.getGeotable());
    LbsSearchServiceImpl service=new LbsSearchServiceImpl(config);
    return service;
  }
  
  public IpService getIpService(){
    Config config=new Config();
    config.setAk(this.config.getAk());
    config.setGeotable(this.config.getGeotable());
    IPServiceImpl service=new IPServiceImpl(config);
    return service;
  }
}
