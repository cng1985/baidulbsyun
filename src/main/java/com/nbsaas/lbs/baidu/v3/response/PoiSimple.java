package com.nbsaas.lbs.baidu.v3.response;

import java.util.Arrays;

public class PoiSimple {
  
  private String title;
  private String address;
  private String tags;
  
  private float[] location;
  
  private float[] gcj_location;
  private String create_time;
  
  private String modify_time;
  
  private String province;
  
  private String city;
  
  private String district;
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public String getTags() {
    return tags;
  }
  
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  
  public String getProvince() {
    return province;
  }
  
  public void setProvince(String province) {
    this.province = province;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getDistrict() {
    return district;
  }
  
  public void setDistrict(String district) {
    this.district = district;
  }
  
  public float[] getLocation() {
    return location;
  }
  
  public void setLocation(float[] location) {
    this.location = location;
  }
  
  public float[] getGcj_location() {
    return gcj_location;
  }
  
  public void setGcj_location(float[] gcj_location) {
    this.gcj_location = gcj_location;
  }
  
  public String getCreate_time() {
    return create_time;
  }
  
  public void setCreate_time(String create_time) {
    this.create_time = create_time;
  }
  
  public String getModify_time() {
    return modify_time;
  }
  
  public void setModify_time(String modify_time) {
    this.modify_time = modify_time;
  }
  
  @Override
  public String toString() {
    return "PoiSimple{" +
        "title='" + title + '\'' +
        ", address='" + address + '\'' +
        ", tags='" + tags + '\'' +
        ", location=" + Arrays.toString(location) +
        ", gcj_location=" + Arrays.toString(gcj_location) +
        ", create_time='" + create_time + '\'' +
        ", modify_time='" + modify_time + '\'' +
        ", province='" + province + '\'' +
        ", city='" + city + '\'' +
        ", district='" + district + '\'' +
        '}';
  }
}
