package com.nbsaas.lbs.baidu.v3.domain;

import java.io.Serializable;

public class State implements Serializable {

	private String message;
	private int status;
	private Long id;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	@Override
	public String toString() {
		return "State [message=" + message + ", status=" + status + ", id="
				+ id + "]";
	}
	
	public Long getId() {
		return id;
	}
	
	public void setId(Long id) {
		this.id = id;
	}
}
