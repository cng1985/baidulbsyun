package com.nbsaas.lbs.baidu.v3.apps;

import com.nbsaas.lbs.baidu.builder.ServiceEngine;
import com.nbsaas.lbs.baidu.builder.ServiceEngineBuilder;
import com.nbsaas.lbs.baidu.v3.domain.State;
import com.nbsaas.lbs.baidu.v3.response.PoiSimple;
import com.nbsaas.lbs.baidu.v3.api.PoiService;
import com.nbsaas.lbs.baidu.v3.domain.Poi;
import com.nbsaas.lbs.baidu.v3.request.PoiListRequest;
import com.nbsaas.lbs.baidu.v3.response.PoiListResponse;
import com.nbsaas.lbs.baidu.v3.service.PoiServiceImpl;
import com.nbsaas.lbs.baidu.webservice.domain.BackPoi;
import com.nbsaas.lbs.baidu.webservice.domain.Backs;
import com.nbsaas.lbs.baidu.webservice.v2.PlaceService;

import java.util.List;

public class PoiApps {
  
  /**
   * @param args
   */
  public static void main(String[] args) {
    a();
  }
  private static void a() {
    PlaceService p = new PlaceService();
    p.setLocation("34.21261,108.910446");
    p.setRadius(2000);
    p.setFilter("industry_type:hotel|sort_name:distance|sort_rule:1");
    Backs b = p.searchByCircle("酒店", 100, 0);
    if (b != null) {
      System.out.println(b.getTotal());
      printPois(b);
    }
  }
  private static void printPois(Backs b) {
    ServiceEngine engine = ServiceEngineBuilder.newBuilder().ak("5UMODI65jVANU8FVkKTt3CfCCUMTA1u0").geotable("191439").build();
    PoiService service =engine.getV3PoiService();
    
    List<BackPoi> pois = b.getResults();
    System.out.println(pois.size());
    for (BackPoi backPoi : pois) {
      System.out.println(backPoi);
      
    }
  }
  
  private static void ad() {
    ServiceEngineBuilder builder = ServiceEngineBuilder.newBuilder();
    builder.ak("5UMODI65jVANU8FVkKTt3CfCCUMTA1u0");
    builder.geotable("191439");
    ServiceEngine serviceEngine = builder.build();
    PoiService service = serviceEngine.getV3PoiService();
    Poi poi = new Poi();
    poi.setAddress("陕西省西安市莲湖区二环路沿线商业经济带北院门115号");
    poi.setTitle("陕西省西安市莲湖区");
    // 108.950524,34.269067
    poi.setLatitude(34.269067);
    poi.setLongitude(108.950524);
    poi.getData().put("name", "ada");
    poi.setCoord_type(3);
    service.add(poi);
  }
  
  private static void list(PoiServiceImpl service) {
    PoiListRequest request = new PoiListRequest();
    request.setTitle("西安紫金山大酒店");
    PoiListResponse pois = service.list(request);
    if (pois.getPois()!=null){
      List<PoiSimple> poiSimples= pois.getPois();
      for (PoiSimple poiSimple : poiSimples) {
        System.out.println(poiSimple);
      }
    }
  }
  
  private static void add(PoiService service) {
    //		Poi poi = new Poi();
//		poi.setAddress("陕西省西安市莲湖区二环路沿线商业经济带北院门115号");
//		poi.setTitle("陕西省西安市莲湖区");
//		// 108.950524,34.269067
//		poi.setLatitude(34.269067);
//		poi.setLongitude(108.950524);
//		poi.getData().put("name", "ada");
//		poi.setCoord_type(3);
//
    
    PlaceService p = new PlaceService();
    p.setRegion("西安");
  
    Backs b = p.searchByKey("酒店", 20, 1);
    if (b != null) {
      List<BackPoi> pois = b.getResults();
      for (BackPoi backPoi : pois) {
        System.out.println(backPoi);
        Poi poi = toPoi(backPoi);
        poi.setCoord_type(3);
        State s = service.add(poi);
        System.out.println(s);
      }
    }
  }
  
  public static Poi toPoi(BackPoi p) {
    Poi result = new Poi();
    result.setAddress(p.getAddress());
    result.setTitle(p.getName());
    if (p.getDetail_info() != null) {
      result.setTags(p.getDetail_info().getTag());
    }
    if (p.getLocation() != null) {
      result.setLatitude(p.getLocation().getLat());
      result.setLongitude(p.getLocation().getLng());
    }
    return result;
  }
}
