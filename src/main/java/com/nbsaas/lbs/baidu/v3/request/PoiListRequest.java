package com.nbsaas.lbs.baidu.v3.request;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class PoiListRequest implements Serializable {
  
  /**
   *  位置数据名称
   */
  private String title;
  
  /**
   * 位置数据类别
   */
  private String tags;
  
  /**
   * 查询的矩形区域,格式x1,y1;x2,y2分别代表矩形的左上角和右下角。
   范围过大时服务易超时，返回数据为空。
   建议经度跨度小于0.8，纬度跨度小于0.5。
   */
  private String bounds;
  
  /**
   * 1：GPS经纬度坐标
   * 2：国测局加密经纬度坐标
   * 3：百度加密经纬度坐标
   * 4：百度加密墨卡托坐标
   */
  private int coordType=3;
  
  private int no=0;
  
  private int size=20;
  
  private Map<String,String> params=new HashMap<String, String>();
  
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getTags() {
    return tags;
  }
  
  public void setTags(String tags) {
    this.tags = tags;
  }
  
  public String getBounds() {
    return bounds;
  }
  
  public void setBounds(String bounds) {
    this.bounds = bounds;
  }
  
  public int getCoordType() {
    return coordType;
  }
  
  public void setCoordType(int coordType) {
    this.coordType = coordType;
  }
  
  public int getNo() {
    return no;
  }
  
  public void setNo(int no) {
    this.no = no;
  }
  
  public int getSize() {
    return size;
  }
  
  public void setSize(int size) {
    this.size = size;
  }
  
  public Map<String, String> getParams() {
    return params;
  }
  
  public void setParams(Map<String, String> params) {
    this.params = params;
  }
}
