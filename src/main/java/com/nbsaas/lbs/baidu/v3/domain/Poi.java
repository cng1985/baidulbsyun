package com.nbsaas.lbs.baidu.v3.domain;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

public class Poi implements Serializable {

	private String title;
	private String address;
	private String tags;
	private double latitude;
	private double longitude;
	private int coord_type;
	private String geotable_id;
	private Map<String,String> data=new HashMap<String, String>();
	public Map<String, String> getData() {
		return data;
	}
	public void setData(Map<String, String> data) {
		this.data = data;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getTags() {
		return tags;
	}
	public void setTags(String tags) {
		this.tags = tags;
	}
	public double getLatitude() {
		return latitude;
	}
	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}
	public double getLongitude() {
		return longitude;
	}
	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}
	public int getCoord_type() {
		return coord_type;
	}
	public void setCoord_type(int coord_type) {
		this.coord_type = coord_type;
	}
	public String getGeotable_id() {
		return geotable_id;
	}
	public void setGeotable_id(String geotable_id) {
		this.geotable_id = geotable_id;
	}
	@Override
	public String toString() {
		return "Poi [title=" + title + ", address=" + address + ", tags="
				+ tags + ", latitude=" + latitude + ", longitude=" + longitude
				+ ", coord_type=" + coord_type + ", geotable_id=" + geotable_id
				+ ", data=" + data + "]";
	}

}
