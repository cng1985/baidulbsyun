package com.nbsaas.lbs.baidu.v3.response;

import java.io.Serializable;

public class PoiUpdateResponse implements Serializable {
  
  private String message;
  private int status;
  
  private String id;
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public int getStatus() {
    return status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  @Override
  public String toString() {
    return "PoiDeleteResponse{" +
        "message='" + message + '\'' +
        ", status=" + status +
        ", id='" + id + '\'' +
        '}';
  }
}
