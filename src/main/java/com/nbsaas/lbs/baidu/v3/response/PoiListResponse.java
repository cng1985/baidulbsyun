package com.nbsaas.lbs.baidu.v3.response;

import java.io.Serializable;
import java.util.List;

public class PoiListResponse implements Serializable {
  
  private String message;
  private int status;
  
  private int size;
  
  private int total;
  
  private List<PoiSimple> pois;
  
  
  public String getMessage() {
    return message;
  }
  
  public void setMessage(String message) {
    this.message = message;
  }
  
  public int getStatus() {
    return status;
  }
  
  public void setStatus(int status) {
    this.status = status;
  }
  
  public int getSize() {
    return size;
  }
  
  public void setSize(int size) {
    this.size = size;
  }
  
  public int getTotal() {
    return total;
  }
  
  public void setTotal(int total) {
    this.total = total;
  }
  
  public List<PoiSimple> getPois() {
    return pois;
  }
  
  public void setPois(List<PoiSimple> pois) {
    this.pois = pois;
  }
  
  @Override
  public String toString() {
    return "PoiListResponse{" +
        "message='" + message + '\'' +
        ", status=" + status +
        ", size=" + size +
        ", total=" + total +
        ", pois=" + pois +
        '}';
  }
}
