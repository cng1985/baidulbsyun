package com.nbsaas.lbs.baidu.ip.service;

import com.nbsaas.lbs.baidu.ip.request.IpRequest;
import com.nbsaas.lbs.baidu.ip.response.IpResponse;

public interface IpService {
  
  IpResponse ip(IpRequest request);
}
