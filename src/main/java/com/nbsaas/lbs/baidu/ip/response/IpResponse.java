package com.nbsaas.lbs.baidu.ip.response;

import java.io.Serializable;

public class IpResponse implements Serializable {
  
  private String address;
  
  private String  status;
  
  private String simpleAddress;
  
  private String city;
  
  private String cityCode;
  
  private String district;
  
  private String province;
  
  private String street;
  
  
  private String streetNumber;
  
  private float latitude;
  private float longitude;
  
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public String getStatus() {
    return status;
  }
  
  public void setStatus(String status) {
    this.status = status;
  }
  
  public String getSimpleAddress() {
    return simpleAddress;
  }
  
  public void setSimpleAddress(String simpleAddress) {
    this.simpleAddress = simpleAddress;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getCityCode() {
    return cityCode;
  }
  
  public void setCityCode(String cityCode) {
    this.cityCode = cityCode;
  }
  
  public String getDistrict() {
    return district;
  }
  
  public void setDistrict(String district) {
    this.district = district;
  }
  
  public String getProvince() {
    return province;
  }
  
  public void setProvince(String province) {
    this.province = province;
  }
  
  public String getStreet() {
    return street;
  }
  
  public void setStreet(String street) {
    this.street = street;
  }
  
  public String getStreetNumber() {
    return streetNumber;
  }
  
  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }
  
  public float getLatitude() {
    return latitude;
  }
  
  public void setLatitude(float latitude) {
    this.latitude = latitude;
  }
  
  public float getLongitude() {
    return longitude;
  }
  
  public void setLongitude(float longitude) {
    this.longitude = longitude;
  }
  
  @Override
  public String toString() {
    return "IpResponse{" +
        "address='" + address + '\'' +
        ", status='" + status + '\'' +
        ", simpleAddress='" + simpleAddress + '\'' +
        ", city='" + city + '\'' +
        ", cityCode='" + cityCode + '\'' +
        ", district='" + district + '\'' +
        ", province='" + province + '\'' +
        ", street='" + street + '\'' +
        ", streetNumber='" + streetNumber + '\'' +
        ", latitude=" + latitude +
        ", longitude=" + longitude +
        '}';
  }
}
