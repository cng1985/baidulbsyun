package com.nbsaas.lbs.baidu.yingyan.domain;

public class Track {

	private String ak;
	private int service_id;

	private double latitude;
	private double longitude;

	/**
	 * 1：GPS经纬度坐标2：国测局加密经纬度坐标 3：百度加密经纬度坐标。
	 */
	private int coord_type = 3;
	
	private String  entity_name ;


	private long  loc_time ;

	public Track(){
		loc_time=System.currentTimeMillis()/1000;
	}
	

	public String getAk() {
		return ak;
	}


	public void setAk(String ak) {
		this.ak = ak;
	}


	public int getService_id() {
		return service_id;
	}


	public void setService_id(int service_id) {
		this.service_id = service_id;
	}


	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(double latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(double longitude) {
		this.longitude = longitude;
	}


	public int getCoord_type() {
		return coord_type;
	}


	public void setCoord_type(int coord_type) {
		this.coord_type = coord_type;
	}


	public String getEntity_name() {
		return entity_name;
	}


	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}


	public long getLoc_time() {
		return loc_time;
	}


	public void setLoc_time(long loc_time) {
		this.loc_time = loc_time;
	}
}
