package com.nbsaas.lbs.baidu.yingyan.service;

import java.io.IOException;

import com.google.gson.Gson;
import com.nbsaas.http.Connection;
import com.nbsaas.http.Connection.Method;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.lbs.baidu.yingyan.domain.Result;
import com.nbsaas.lbs.baidu.yingyan.domain.Track;

public class TrackService {

	private String ADD = "http://api.map.baidu.com/trace/v2/track/addpoint";
	private String DELETE = "http://api.map.baidu.com/trace/v2/entity/delete";
	private String UPDATE = "http://api.map.baidu.com/trace/v2/entity/update";

	public Result add(Track entity) {
		Result result=null;
		Connection connection = HttpConnection.connect(ADD);
		connection.method(Method.POST);
		connection.data("ak", entity.getAk());
		connection.data("entity_name", entity.getEntity_name());
		connection.data("service_id", "" + entity.getService_id());
		connection.data("latitude", "" + entity.getLatitude());
		connection.data("longitude", "" + entity.getLongitude());
		connection.data("coord_type", "" + entity.getCoord_type());
		connection.data("loc_time", "" + entity.getLoc_time());

		try {
			String body = connection.execute().body();
			Gson gson=new Gson();
			result=gson.fromJson(body, Result.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}
}
