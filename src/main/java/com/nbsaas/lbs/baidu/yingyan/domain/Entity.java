package com.nbsaas.lbs.baidu.yingyan.domain;

import java.io.Serializable;

public class Entity implements Serializable{

	private String ak;
	private int  service_id ;
	private String  entity_name ;
	public String getAk() {
		return ak;
	}
	public void setAk(String ak) {
		this.ak = ak;
	}
	public int getService_id() {
		return service_id;
	}
	public void setService_id(int service_id) {
		this.service_id = service_id;
	}
	public String getEntity_name() {
		return entity_name;
	}
	public void setEntity_name(String entity_name) {
		this.entity_name = entity_name;
	}
	
}
