package com.nbsaas.lbs.baidu.yingyan.service;

import java.io.IOException;

import com.google.gson.Gson;
import com.nbsaas.http.Connection;
import com.nbsaas.http.Connection.Method;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.lbs.baidu.yingyan.domain.Entity;
import com.nbsaas.lbs.baidu.yingyan.domain.Result;

public class Entityservice {
	private String ADD = "http://api.map.baidu.com/trace/v2/entity/add";
	private String DELETE = "http://api.map.baidu.com/trace/v2/entity/delete";
	private String UPDATE = "http://api.map.baidu.com/trace/v2/entity/update";

	
	public Result add(Entity entity) {
		Result result=null;
		Connection connection = HttpConnection.connect(ADD);
		connection.method(Method.POST);
		connection.data("ak", entity.getAk());
		connection.data("entity_name", entity.getEntity_name());
		connection.data("service_id", "" + entity.getService_id());
		try {
			String body = connection.execute().body();
			Gson gson=new Gson();
			result=gson.fromJson(body, Result.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}
	public Result delete(Entity entity) {
		Result result=null;
		Connection connection = HttpConnection.connect(DELETE);
		connection.method(Method.POST);
		connection.data("ak", entity.getAk());
		connection.data("entity_name", entity.getEntity_name());
		connection.data("service_id", "" + entity.getService_id());
		try {
			String body = connection.execute().body();
			Gson gson=new Gson();
			result=gson.fromJson(body, Result.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}
	public Result update(Entity entity) {
		Result result=null;
		Connection connection = HttpConnection.connect(UPDATE);
		connection.method(Method.POST);
		connection.data("ak", entity.getAk());
		connection.data("entity_name", entity.getEntity_name());
		connection.data("service_id", "" + entity.getService_id());
		try {
			String body = connection.execute().body();
			Gson gson=new Gson();
			result=gson.fromJson(body, Result.class);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return result;

	}
	
}
