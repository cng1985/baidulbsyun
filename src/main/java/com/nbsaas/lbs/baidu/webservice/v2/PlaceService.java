package com.nbsaas.lbs.baidu.webservice.v2;

import com.google.gson.Gson;
import com.nbsaas.http.Connection;
import com.nbsaas.http.Connection.Method;
import com.nbsaas.http.HttpConnection;
import com.nbsaas.lbs.baidu.webservice.domain.BackPoi;
import com.nbsaas.lbs.baidu.webservice.domain.Backs;

import java.io.IOException;
import java.util.List;

public class PlaceService {
  
  String urlsearch = "http://api.map.baidu.com/place/v2/search";
  
  public String getRegion() {
    return region;
  }
  
  public void setRegion(String region) {
    this.region = region;
  }
  
  public int getRadius() {
    return radius;
  }
  
  public void setRadius(int radius) {
    this.radius = radius;
  }
  
  public String getLocation() {
    return location;
  }
  
  public void setLocation(String location) {
    this.location = location;
  }
  
  public String getBounds() {
    return bounds;
  }
  
  public void setBounds(String bounds) {
    this.bounds = bounds;
  }
  
  public static void main(String[] args) {
    PlaceService s = new PlaceService();
    s.region = "西安";
    s.radius = 100;
    s.location = "39.915,116.404,39.975,116.414";
    s.bounds = "39.915,116.404,39.975,116.414";
    Backs b = s.searchByKey("美食", 200, 1);
    if (b != null) {
      List<BackPoi> pois = b.getResults();
      for (BackPoi backPoi : pois) {
        System.out.println(backPoi);
      }
    }
  }
  
  private String region;
  private int radius;
  private String location;
  private String bounds;
  
  private String filter;
  
  public String getFilter() {
    return filter;
  }
  
  public void setFilter(String filter) {
    this.filter = filter;
  }
  
  public Backs searchByKey(String keyword, int page_size, int page_num) {
    Backs reult = null;
    Connection connection = HttpConnection.connect(urlsearch);
    connection.data("q", keyword);
    // json或xml
    connection.data("output", "json");
    // connection.data("tag", keyword); 日式烧烤/铁板烧、朝外大街
    // 检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
    connection.data("scope", "2");
    connection.data("page_size", "" + page_size);
    connection.data("page_num", "" + page_num);
    connection.data("region", "" + region);
    
    connection.data("ak", ServiceConfig.ak);
    if (filter != null) {
      connection.data("filter", "" + filter);
    }
    connection.method(Method.GET);
    
    try {
      String back = connection.execute().body();
      Gson gson = new Gson();
      reult = gson.fromJson(back, Backs.class);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return reult;
    
  }
  
  public Backs searchByCircle(String keyword, int page_size, int page_num) {
    Backs reult = null;
    Connection connection = HttpConnection.connect(urlsearch);
    connection.data("q", keyword);
    // json或xml
    connection.data("output", "json");
    // connection.data("tag", keyword); 日式烧烤/铁板烧、朝外大街
    // 检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
    connection.data("scope", "2");
    connection.data("page_size", "" + page_size);
    connection.data("page_num", "" + page_num);
    connection.data("radius", "" + radius);
    connection.data("location", "" + location);
    if (filter != null) {
      connection.data("filter", "" + filter);
    }
    
    
    connection.data("ak", ServiceConfig.ak);
    connection.method(Method.GET);
    try {
      String back = connection.execute().body();
      Gson gson = new Gson();
      reult = gson.fromJson(back, Backs.class);
    } catch (IOException e) {
      // TODO Auto-generated catch block
      e.printStackTrace();
    }
    return reult;
    
  }
  
  public Backs searchByBounds(String keyword) {
    Backs reult = null;
    Connection connection = HttpConnection.connect(urlsearch);
    connection.data("q", keyword);
    // json或xml
    connection.data("output", "json");
    // connection.data("tag", keyword); 日式烧烤/铁板烧、朝外大街
    // 检索结果详细程度。取值为1 或空，则返回基本信息；取值为2，返回检索POI详细信息
    connection.data("scope", "2");
    //connection.data("page_size", "" + page_size);
    //connection.data("page_num", "" + page_num);
    connection.data("bounds", "" + bounds);
    connection.data("ak", ServiceConfig.ak);
    if (filter != null) {
      connection.data("filter", "" + filter);
      connection.method(Method.GET);
    }
    try {
      String back = connection.execute().body();
      Gson gson = new Gson();
      reult = gson.fromJson(back, Backs.class);
    } catch (IOException e) {
      e.printStackTrace();
    }
    return reult;
    
  }
  
}
