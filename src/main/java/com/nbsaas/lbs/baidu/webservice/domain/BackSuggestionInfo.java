package com.nbsaas.lbs.baidu.webservice.domain;

import java.io.Serializable;

public class BackSuggestionInfo implements Serializable{

	private String name;
	private String city;
	private String district;
	private String business;
	private String cityid;
	public String getName() {
		return name;
	}
	@Override
	public String toString() {
		return "BackSuggestionInfo [name=" + name + ", city=" + city
				+ ", district=" + district + ", business=" + business
				+ ", cityid=" + cityid + "]";
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getBusiness() {
		return business;
	}
	public void setBusiness(String business) {
		this.business = business;
	}
	public String getCityid() {
		return cityid;
	}
	public void setCityid(String cityid) {
		this.cityid = cityid;
	}

}
