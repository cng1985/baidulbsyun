package com.nbsaas.lbs.baidu.webservice.domain;

import java.io.Serializable;

public class BackLocation implements Serializable {

	/**
	 * 纬度
	 */
	private float lat;

	/**
	 * 经度
	 */
	private float lng;

	public String getgps() {
		return  lng+ "," + lat;
	}

	public float getLat() {
		return lat;
	}

	public float getLng() {
		return lng;
	}
	
	public void setLat(float lat) {
		this.lat = lat;
	}

	public void setLng(float lng) {
		this.lng = lng;
	}
	
	@Override
	public String toString() {
		return  lng+ "," + lat;
	}
}
