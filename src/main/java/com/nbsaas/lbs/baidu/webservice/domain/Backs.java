package com.nbsaas.lbs.baidu.webservice.domain;

import java.io.Serializable;
import java.util.List;

public class Backs implements Serializable {

	private int status;
	private String message;
	private int total;
	List<BackPoi> results;
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public int getTotal() {
		return total;
	}
	public void setTotal(int total) {
		this.total = total;
	}
	public List<BackPoi> getResults() {
		return results;
	}
	public void setResults(List<BackPoi> results) {
		this.results = results;
	}
	@Override
	public String toString() {
		return "Backs [status=" + status + ", message=" + message + ", total="
				+ total + ", results=" + results + "]";
	}
}
