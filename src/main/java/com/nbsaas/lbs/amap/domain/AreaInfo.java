package com.nbsaas.lbs.amap.domain;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AreaInfo implements Serializable, Comparable<AreaInfo> {
  
  private String name;
  
  private String code;
  
  private String level;
  
  private String center;
  
  private List<AreaInfo> childs=new ArrayList<>();
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getCode() {
    return code;
  }
  
  public void setCode(String code) {
    this.code = code;
  }
  
  public String getLevel() {
    return level;
  }
  
  public void setLevel(String level) {
    this.level = level;
  }
  
  public String getCenter() {
    return center;
  }
  
  public void setCenter(String center) {
    this.center = center;
  }
  
  public List<AreaInfo> getChilds() {
    return childs;
  }
  
  public void setChilds(List<AreaInfo> childs) {
    this.childs = childs;
  }
  
  @Override
  public String toString() {
    return "AreaInfo{" +
        "name='" + name + '\'' +
        ", code='" + code + '\'' +
        ", level='" + level + '\'' +
        ", center='" + center + '\'' +
        '}';
  }
  
  @Override
  public int compareTo(AreaInfo o) {
    return this.getCode().compareTo(o.getCode());
  }
}
