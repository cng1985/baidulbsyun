package com.nbsaas.lbs.amap.exams;


import com.nbsaas.lbs.amap.v3.service.DistrictService;
import com.nbsaas.lbs.amap.v3.service.impl.DistrictServiceImpl;
import com.nbsaas.lbs.baidu.v3.service.Config;

public class App {
  public static void main(String[] args) {
    Config config=new Config();
    config.setAk("c3ed521babcc6c3b1af2467064d216d7");
    DistrictService service=new DistrictServiceImpl(config);
    service.area("110114010").stream().forEach(item-> System.out.println(item));
  }
}
