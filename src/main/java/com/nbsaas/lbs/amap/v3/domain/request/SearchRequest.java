package com.nbsaas.lbs.amap.v3.domain.request;

public class SearchRequest {
  
  
  private String keywords;
  
  /**
   * 0：不返回下级行政区；
   * <p>
   * 1：返回下一级行政区；
   * <p>
   * 2：返回下两级行政区；
   * <p>
   * 3：返回下三级行政区；
   */
  private String subdistrict;
  
  private int no = 1;
  
  private int size = 20;
  
  public String getKeywords() {
    return keywords;
  }
  
  public void setKeywords(String keywords) {
    this.keywords = keywords;
  }
  
  public String getSubdistrict() {
    return subdistrict;
  }
  
  public void setSubdistrict(String subdistrict) {
    this.subdistrict = subdistrict;
  }
  
  public int getNo() {
    return no;
  }
  
  public void setNo(int no) {
    this.no = no;
  }
  
  public int getSize() {
    return size;
  }
  
  public void setSize(int size) {
    this.size = size;
  }
}
