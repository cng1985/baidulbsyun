package com.nbsaas.lbs.qq.district.app;

import com.google.gson.Gson;
import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;
import com.nbsaas.lbs.qq.district.resource.DistrictResource;
import jodd.http.HttpRequest;
import jodd.http.HttpResponse;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {
    public static void main(String[] args) {

        DistrictResource districtResource = new DistrictResource();
        ListResponse<List<DistrictSimple>> data = districtResource.list();
        List<AreaRequest> requests=new ArrayList<>();
        for (DistrictSimple datum : data.getData().get(0)) {

            AreaRequest temp=new AreaRequest();
            temp.setName(datum.getName());
            temp.setCode(datum.getId());
            temp.setFullName(datum.getFullname());
            temp.setLat(datum.getLocation().getLat());
            temp.setLng(datum.getLocation().getLng());
            requests.add(temp);
        }
        Map<String, Object> datum =new HashMap<>();
        datum.put("items",requests);
        HttpRequest request = HttpRequest.post("http://192.168.0.120:8100/action/area/syncRoot");
        request.contentType("application/json");
        request.charset("utf-8");
        //参数详情
        String body = new Gson().toJson(datum);
        request.body(body);
        request.bodyText(body, "application/json", "utf-8");

        HttpResponse response = request.send();
        String respJson = response.bodyText();
        System.out.println(respJson);
    }
}
