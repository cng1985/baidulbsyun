package com.nbsaas.lbs.qq.district.domain.simple;

import lombok.Data;

import java.util.List;

@Data
public class DistrictResult {

    private List<List<DistrictSimple>> result;
}
