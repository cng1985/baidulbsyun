package com.nbsaas.lbs.qq.district.resource;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class Md5Utils {
    public static String getMD5Hash(String input) {
        try {
            // 获取MD5实例
            MessageDigest md = MessageDigest.getInstance("MD5");
            // 将字符串转换为字节数组并更新摘要
            md.update(input.getBytes());
            // 计算摘要（哈希值）
            byte[] digest = md.digest();
            // 将字节数组转换为16进制格式的字符串
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b));
            }
            return sb.toString();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 algorithm not found", e);
        }
    }
}
