package com.nbsaas.lbs.qq.district.app;

import lombok.Data;

import java.util.List;

@Data
public class AreaData {

    private List<AreaRequest> data;
}
