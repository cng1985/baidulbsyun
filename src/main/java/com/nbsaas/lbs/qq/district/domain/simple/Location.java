package com.nbsaas.lbs.qq.district.domain.simple;

import lombok.Data;

@Data
public class Location {

    private Double lat;

    private Double lng;
}
