package com.nbsaas.lbs.qq.district.api;

import com.nbsaas.boot.rest.response.ListResponse;
import com.nbsaas.lbs.qq.district.domain.simple.DistrictSimple;

import java.util.List;

public interface DistrictApi {

    ListResponse<DistrictSimple> root();

    ListResponse<List<DistrictSimple>> list();

    ListResponse<DistrictSimple> children(String code);

}
