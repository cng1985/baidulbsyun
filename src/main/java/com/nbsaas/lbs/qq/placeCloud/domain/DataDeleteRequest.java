package com.nbsaas.lbs.qq.placeCloud.domain;

import com.google.gson.annotations.SerializedName;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class DataDeleteRequest implements Serializable {

    private String key;

    @SerializedName("table_id")
    private String tableId;

    private String id;

    private String filter;
}
