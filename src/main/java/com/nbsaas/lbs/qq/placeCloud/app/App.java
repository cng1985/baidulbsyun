package com.nbsaas.lbs.qq.placeCloud.app;

import com.nbsaas.lbs.qq.placeCloud.api.DataService;
import com.nbsaas.lbs.qq.placeCloud.domain.DataItem;
import com.nbsaas.lbs.qq.placeCloud.domain.DataUpdateRequest;
import com.nbsaas.lbs.qq.v1.builder.ServicesBuilder;
import com.nbsaas.lbs.qq.v1.domain.request.SearchByNearbyRequest;
import com.nbsaas.lbs.qq.v1.domain.response.Poi;
import com.nbsaas.lbs.qq.v1.domain.response.Pois;
import com.nbsaas.lbs.qq.v1.service.PlaceService;

import java.util.ArrayList;
import java.util.List;

public class App {

    public static void main(String[] args) {

        DataService dataService=new DataService();
        PlaceService service = ServicesBuilder.newBuilder().key("LUCBZ-NE5KK-7UEJH-AFXWU-3Q6SO-PPFQI").build().getPlaceService();
        SearchByNearbyRequest request = new SearchByNearbyRequest();
        request.setKeyword("酒店");
        request.setNo(1);
//    request.setLat(34.33504f);
//    request.setLng(108.94778f);
//    request.setRadius(3000);
        Pois pois = service.searchByNearby(request);
        List<Poi> datas = pois.getData();
        Integer i=0;
        for (Poi data : datas) {
            System.out.println(data);
            DataUpdateRequest createRequest=new DataUpdateRequest();
            createRequest.setKey("LUCBZ-NE5KK-7UEJH-AFXWU-3Q6SO-PPFQI");
            createRequest.setTableId("6047172d702f8b4c535a1a5c");
            List<DataItem> items=new ArrayList<>();
            DataItem item=new DataItem();
            item.setId(""+i);
            item.setTitle(data.getTitle());
            //item.setLocation(data.getLocation());
            item.setTel("11");
            items.add(item);
            createRequest.setItems(items);
            dataService.update(createRequest);
            i++;
        }
    }
}
