package com.nbsaas.lbs.qq.placeCloud.api;

import com.nbsaas.lbs.qq.placeCloud.domain.BackResponse;
import com.nbsaas.lbs.qq.placeCloud.domain.DataCreateRequest;
import com.nbsaas.lbs.qq.placeCloud.domain.DataDeleteRequest;
import com.nbsaas.lbs.qq.placeCloud.domain.DataUpdateRequest;
import com.nbsaas.lbs.qq.v1.domain.response.Poi;

import java.io.IOException;
import java.util.List;

public interface DataApi {


    BackResponse create(DataCreateRequest request) throws IOException;

    BackResponse update(DataUpdateRequest request);

    BackResponse delete(DataDeleteRequest request);


    List<Poi> list(DataDeleteRequest request);

}
