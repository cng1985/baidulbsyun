package com.nbsaas.lbs.qq.v1.domain.request;

public class SearchByCityRequest  extends BaseRequest{
  
  private String city;
  

  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
 
}
