package com.nbsaas.lbs.qq.v1.domain.response;

import java.io.Serializable;

public class AreaItem implements Serializable {
  
  private String id;
  
  private String name;
  
  private String fullname;
  
  private Location location;
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getName() {
    return name;
  }
  
  public void setName(String name) {
    this.name = name;
  }
  
  public String getFullname() {
    return fullname;
  }
  
  public void setFullname(String fullname) {
    this.fullname = fullname;
  }
  
  public Location getLocation() {
    return location;
  }
  
  public void setLocation(Location location) {
    this.location = location;
  }
  
  @Override
  public String toString() {
    return "AreaItem{" +
        "id='" + id + '\'' +
        ", name='" + name + '\'' +
        ", fullname='" + fullname + '\'' +
        ", location=" + location +
        '}'+"\n";
  }
}
