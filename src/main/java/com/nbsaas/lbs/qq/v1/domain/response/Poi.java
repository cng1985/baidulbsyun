package com.nbsaas.lbs.qq.v1.domain.response;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Poi  implements Serializable {
  private String id;
  
  private String title;
  
  private String address;
  private String tel;
  private String category;
  private String type;
  private String boundary;
  
  private Location location;

  @SerializedName("_distance")
  private Double distance;
  
  private AreaInfo ad_info;
  
  public String getId() {
    return id;
  }
  
  public void setId(String id) {
    this.id = id;
  }
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public String getAddress() {
    return address;
  }
  
  public void setAddress(String address) {
    this.address = address;
  }
  
  public String getTel() {
    return tel;
  }
  
  public void setTel(String tel) {
    this.tel = tel;
  }
  
  public String getCategory() {
    return category;
  }
  
  public void setCategory(String category) {
    this.category = category;
  }
  
  public String getType() {
    return type;
  }
  
  public void setType(String type) {
    this.type = type;
  }
  
  public String getBoundary() {
    return boundary;
  }
  
  public void setBoundary(String boundary) {
    this.boundary = boundary;
  }
  
  public Location getLocation() {
    return location;
  }
  
  public void setLocation(Location location) {
    this.location = location;
  }

  public Double getDistance() {
    return distance;
  }

  public void setDistance(Double distance) {
    this.distance = distance;
  }

  @Override
  public String toString() {
    return "Poi{" +
        "id='" + id + '\'' +
        ", title='" + title + '\'' +
        ", address='" + address + '\'' +
        ", tel='" + tel + '\'' +
        ", category='" + category + '\'' +
        ", type='" + type + '\'' +
        ", boundary='" + boundary + '\'' +
        ", location=" + location +
        '}';
  }
  
  public AreaInfo getAd_info() {
    return ad_info;
  }
  
  public void setAd_info(AreaInfo ad_info) {
    this.ad_info = ad_info;
  }
}
