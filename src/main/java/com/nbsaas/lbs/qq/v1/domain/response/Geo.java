package com.nbsaas.lbs.qq.v1.domain.response;

import java.io.Serializable;

public class Geo implements Serializable {
  
  private String title;
  
  private Location location;
  
  private Float similarity;
  
  private Float reliability;
  private Float deviation;
  /**
   * 1 	城市
   * 2 	区、县
   * 3 	乡镇、街道
   * 4 	村、社区
   * 5 	开发区
   * 6 	热点区域、商圈
   * 7 	道路
   * 8 	道路附属点：交叉口、收费站、出入口等
   * 9 	门址
   * 10 	小区、大厦
   * 11 	POI点
   */
  private Integer level;
  
  
  private String province;// 	 	 	省
  private String city;// 	 	 	市
  private String district ;//		区，可能为空字串
  private String street;// 	街道，可能为空字串
  private String streetNumber;//门牌，可能为空字串
  
  public String getTitle() {
    return title;
  }
  
  public void setTitle(String title) {
    this.title = title;
  }
  
  public Location getLocation() {
    return location;
  }
  
  public void setLocation(Location location) {
    this.location = location;
  }
  
  public Float getSimilarity() {
    return similarity;
  }
  
  public void setSimilarity(Float similarity) {
    this.similarity = similarity;
  }
  
  public Float getReliability() {
    return reliability;
  }
  
  public void setReliability(Float reliability) {
    this.reliability = reliability;
  }
  
  public Float getDeviation() {
    return deviation;
  }
  
  public void setDeviation(Float deviation) {
    this.deviation = deviation;
  }
  
  public Integer getLevel() {
    return level;
  }
  
  public void setLevel(Integer level) {
    this.level = level;
  }
  
  public String getProvince() {
    return province;
  }
  
  public void setProvince(String province) {
    this.province = province;
  }
  
  public String getCity() {
    return city;
  }
  
  public void setCity(String city) {
    this.city = city;
  }
  
  public String getDistrict() {
    return district;
  }
  
  public void setDistrict(String district) {
    this.district = district;
  }
  
  public String getStreet() {
    return street;
  }
  
  public void setStreet(String street) {
    this.street = street;
  }
  
  public String getStreetNumber() {
    return streetNumber;
  }
  
  public void setStreetNumber(String streetNumber) {
    this.streetNumber = streetNumber;
  }
  
  @Override
  public String toString() {
    return "Geo{" +
        "title='" + title + '\'' +
        ", location=" + location +
        ", similarity=" + similarity +
        ", reliability=" + reliability +
        ", deviation=" + deviation +
        ", level=" + level +
        ", province='" + province + '\'' +
        ", city='" + city + '\'' +
        ", district='" + district + '\'' +
        ", street='" + street + '\'' +
        ", streetNumber='" + streetNumber + '\'' +
        '}';
  }
}
