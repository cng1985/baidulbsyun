package com.nbsaas.lbs.qq.v1.builder;

public class ServicesBuilder {
  
  private String key;
  
  
  private ServicesBuilder(){
  }
  
  public Services build() {
    Services result = new Services(key);
    return result;
  }
  
  public static ServicesBuilder newBuilder() {
    ServicesBuilder result = new ServicesBuilder();
    return result;
  }
  
  public ServicesBuilder key(String key) {
    this.key = key;
    return this;
  }
}
