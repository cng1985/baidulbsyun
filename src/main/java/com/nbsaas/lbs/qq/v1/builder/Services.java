package com.nbsaas.lbs.qq.v1.builder;

import com.nbsaas.lbs.qq.v1.service.DistrictService;
import com.nbsaas.lbs.qq.v1.service.GeoCoderService;
import com.nbsaas.lbs.qq.v1.service.PlaceService;

public class Services {
  
  public Services(String key) {
    this.key = key;
  }
  
  private String key;
  
  public DistrictService getDistrictService() {
    return new DistrictService(key);
  }
  
  public GeoCoderService geoGeoCoderService() {
    return new GeoCoderService(key);
  }
  
  public PlaceService getPlaceService() {
    return new PlaceService(key);
  }
}
