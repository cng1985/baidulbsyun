package com.nbsaas.lbs.qq.v1.domain.request;

public class BaseRequest {
  
  private String keyword;
  
  private Integer no=1;
  
  private Integer size=20;
  
  private String orderby="_distance";
  
  private String key="H4DBZ-WLVCU-YLEVF-4MIDF-MGB5H-TOFDR";
  
  public String getKeyword() {
    return keyword;
  }
  
  public void setKeyword(String keyword) {
    this.keyword = keyword;
  }
  
  public Integer getNo() {
    return no;
  }
  
  public void setNo(Integer no) {
    this.no = no;
  }
  
  public Integer getSize() {
    return size;
  }
  
  public void setSize(Integer size) {
    this.size = size;
  }
  
  public String getOrderby() {
    return orderby;
  }
  
  public void setOrderby(String orderby) {
    this.orderby = orderby;
  }
  
  public String getKey() {
    return key;
  }
  
  public void setKey(String key) {
    this.key = key;
  }
}
