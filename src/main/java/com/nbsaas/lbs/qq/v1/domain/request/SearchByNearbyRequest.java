package com.nbsaas.lbs.qq.v1.domain.request;

public class SearchByNearbyRequest extends BaseRequest {

    private Float lat;

    private Float lng;

    private Integer radius;

    public Float getLat() {
        if (lat == null) {
            return 34.33504f;
        }
        return lat;
    }

    public void setLat(Float lat) {
        this.lat = lat;
    }

    public Float getLng() {
        if (lng == null) {
            return 108.94778f;
        }
        return lng;
    }

    public void setLng(Float lng) {
        this.lng = lng;
    }

    public Integer getRadius() {
        if (radius == null) {
            return 3000;
        }
        return radius;
    }

    public void setRadius(Integer radius) {
        this.radius = radius;
    }
}
