package com.nbsaas.lbs.qq.exams;

import com.nbsaas.lbs.qq.v1.builder.ServicesBuilder;
import com.nbsaas.lbs.qq.v1.domain.request.SearchByNearbyRequest;
import com.nbsaas.lbs.qq.v1.domain.response.Poi;
import com.nbsaas.lbs.qq.v1.domain.response.Pois;
import com.nbsaas.lbs.qq.v1.service.PlaceService;

import java.util.List;

public class App {
    public static void main(String[] args) {
        PlaceService service = ServicesBuilder.newBuilder().key("LUCBZ-NE5KK-7UEJH-AFXWU-3Q6SO-PPFQI").build().getPlaceService();
        SearchByNearbyRequest request = new SearchByNearbyRequest();
        request.setKeyword("医院");
        request.setNo(1);
        request.setSize(100);
//    request.setLat(34.33504f);
//    request.setLng(108.94778f);
//    request.setRadius(3000);
        Pois pois = service.searchByNearby(request);
        List<Poi> datas = pois.getData();
        for (Poi data : datas) {
            System.out.println(data);
        }
    }
}
