package com.nbsaas.utils;

public class StringUtils {
  
  public static boolean isEmpty(CharSequence string) {
    return string == null || string.length() == 0;
  }
  
  public static boolean isAllEmpty(String... strings) {
    String[] var1 = strings;
    int var2 = strings.length;
    
    for(int var3 = 0; var3 < var2; ++var3) {
      String string = var1[var3];
      if (!isEmpty(string)) {
        return false;
      }
    }
    
    return true;
  }
  
  public static boolean isBlank(CharSequence string) {
    return string == null || containsOnlyWhitespaces(string);
  }
  
  public static boolean isNotBlank(String string) {
    return string != null && !containsOnlyWhitespaces(string);
  }
  public static boolean containsOnlyWhitespaces(CharSequence string) {
    int size = string.length();
    
    for(int i = 0; i < size; ++i) {
      char c = string.charAt(i);
      if (!CharUtil.isWhitespace(c)) {
        return false;
      }
    }
    
    return true;
  }
}
