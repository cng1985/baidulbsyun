# baidulbsyun

#### 项目介绍
百度云lbs webservice    api


#### 使用说明

1. 添加poi信息

```
ServiceEngineBuilder builder = ServiceEngineBuilder.newBuilder();
builder.ak("you ak");
builder.geotable("you geotable");
ServiceEngine serviceEngine = builder.build();
PoiService service = serviceEngine.getV3PoiService();
Poi poi = new Poi();
poi.setAddress("陕西省西安市莲湖区二环路沿线商业经济带北院门115号");
poi.setTitle("陕西省西安市莲湖区");
// 108.950524,34.269067
poi.setLatitude(34.269067);
poi.setLongitude(108.950524);
poi.getData().put("name", "ada");
poi.setCoord_type(3);
service.add(poi);
```

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
